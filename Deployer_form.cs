﻿using System;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Patch_deployer
{
    public partial class Patch_deployer : Form
    {
        //Создадим конфиг
        public class recConfig {
            public string working_dir_path { get; set; } //Рабочий каталог
            public string eclipse_path { get; set; } //Путь до CFT_IDE
            public string workspace_path { get; set; } //Путь к рабочей директории для сборки патча
            public string tns_list { get; set; } //Путь до TNS
            public string RegexpList { get; set; } //Регулярное выражение для составления списка схем
            public string patch_path_pck { get; set; } //Путь к pck
            public string scheme { get; set; }  //Схема
            public string owner { get; set; }   //Владелец схемы
            public string user { get; set; }    //Пользователь
            public string pass { get; set; }    //Пароль
            public string mode { get; set; }    //Режим работы приложения
            public bool old { get; set; }       //Выгружать Old патч
        }

        recConfig config = new recConfig();

        //Читаем файл конфигурации
        private void readConfig()
        {
            NameValueCollection sAll;
            sAll = ConfigurationManager.AppSettings;

            config.working_dir_path = sAll.Get("WORKING_DIR_PATH");
            config.eclipse_path = sAll.Get("ECLIPSE_PATH");
            config.workspace_path = sAll.Get("WORKSPACE_PATH");
            config.tns_list = sAll.Get("TNS_LIST");
            config.RegexpList = sAll.Get("REGEXP_TNS_LIST");
            config.owner = txtOwner.Text = sAll.Get("OWNER_DEFAULT");
            config.user = txtUser.Text = sAll.Get("USER_DEFAULT");
        }

        //Инициализируем компоненты и считываем конфиг
        public Patch_deployer()
        {
            InitializeComponent();
            readConfig();
        }

        //События при загрузке формы
        private void Patch_deployer_Load(object sender, EventArgs e)
        {
            shemaCmbox.DropDown += new EventHandler(shemaCmbox_Fill);
            modeCmbox.DropDown += new EventHandler(modeCmbox_Fill);

            if (!File.Exists(Path.Combine(config.eclipse_path)))
            {
                MessageBox.Show("Не найден eclipsec.exe. Проверьте файл конфигурации.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        //Указать путь к pck
        private void btnPckPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog pck_select = new OpenFileDialog() { Filter = "All files (*.*)|*.pck" };
            if (pck_select.ShowDialog() == DialogResult.OK)
            {
                config.patch_path_pck = txtPckPath.Text = pck_select.FileName;
            }
        }

        //Заполнение дропбокса схемы
        private void shemaCmbox_Fill(object sender, EventArgs e)
        {
            shemaCmbox.Items.Clear();
            config.scheme = "";
            if (File.Exists(config.tns_list))
            {
                using (StreamReader sr = new StreamReader(config.tns_list))
                {
                    if (!String.IsNullOrWhiteSpace(config.RegexpList))
                    {
                        Regex regex = new Regex(config.RegexpList);
                        while (!sr.EndOfStream)
                        {
                            MatchCollection matches = regex.Matches(sr.ReadLine());
                            foreach (Match match in matches)
                            {
                                shemaCmbox.Items.Add(match.Value);
                            }
                        }
                    }
                    else
                    {
                        while (!sr.EndOfStream)
                            shemaCmbox.Items.Add(sr.ReadLine());
                    }
                }
            }
        }

        //Заполняем схему в конфиг
        private void shemaCmbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            config.scheme = shemaCmbox.SelectedItem.ToString();
        }

        //Заполнение дропбокса режима
        private void modeCmbox_Fill(object sender, EventArgs e)
        {
            modeCmbox.Items.Clear();
            config.mode = "";
            string[] mode_arr = { "DEPLOY", "EXPORT" };
            modeCmbox.Items.AddRange(mode_arr);
        }

        //Заполняем режим в конфиг
        private void modeCmbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            config.mode = modeCmbox.SelectedItem.ToString();
        }

        public void BtnOk_Click(object sender, EventArgs e)
        {
            // Process.Start(Path.Combine(config.eclipse_path), " /cf " + Path.Combine(pathName, nameFiles + ".xml") + " /p " + config.pass);
            config.owner = txtOwner.Text;
            config.user = txtUser.Text;
            config.pass = txtPass.Text;
            config.old = oldChkbox.Checked;

            Process.Start(config.eclipse_path);

            this.Close();
        }

        public void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}