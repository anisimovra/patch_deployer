﻿namespace Patch_deployer
{
    partial class Patch_deployer
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.oldChkbox = new System.Windows.Forms.CheckBox();
            this.txtOwner = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.modeCmbox = new System.Windows.Forms.ComboBox();
            this.btnPckPath = new System.Windows.Forms.Button();
            this.txtPckPath = new System.Windows.Forms.TextBox();
            this.shemaCmbox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(179, 201);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 0;
            this.BtnOk.Text = "ОК";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(260, 201);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // oldChkbox
            // 
            this.oldChkbox.AutoSize = true;
            this.oldChkbox.Location = new System.Drawing.Point(226, 167);
            this.oldChkbox.Name = "oldChkbox";
            this.oldChkbox.Size = new System.Drawing.Size(91, 17);
            this.oldChkbox.TabIndex = 2;
            this.oldChkbox.Text = "Собирать old";
            this.oldChkbox.UseVisualStyleBackColor = true;
            // 
            // txtOwner
            // 
            this.txtOwner.Location = new System.Drawing.Point(108, 78);
            this.txtOwner.Name = "txtOwner";
            this.txtOwner.Size = new System.Drawing.Size(227, 20);
            this.txtOwner.TabIndex = 3;
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(108, 104);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(227, 20);
            this.txtUser.TabIndex = 4;
            // 
            // modeCmbox
            // 
            this.modeCmbox.FormattingEnabled = true;
            this.modeCmbox.Location = new System.Drawing.Point(108, 165);
            this.modeCmbox.Name = "modeCmbox";
            this.modeCmbox.Size = new System.Drawing.Size(112, 21);
            this.modeCmbox.TabIndex = 5;
            this.modeCmbox.SelectedIndexChanged += new System.EventHandler(this.modeCmbox_SelectedIndexChanged);
            // 
            // btnPckPath
            // 
            this.btnPckPath.Location = new System.Drawing.Point(12, 12);
            this.btnPckPath.Name = "btnPckPath";
            this.btnPckPath.Size = new System.Drawing.Size(90, 23);
            this.btnPckPath.TabIndex = 6;
            this.btnPckPath.Text = "Выбрать PCK";
            this.btnPckPath.UseVisualStyleBackColor = true;
            this.btnPckPath.Click += new System.EventHandler(this.btnPckPath_Click);
            // 
            // txtPckPath
            // 
            this.txtPckPath.Location = new System.Drawing.Point(108, 14);
            this.txtPckPath.Name = "txtPckPath";
            this.txtPckPath.Size = new System.Drawing.Size(231, 20);
            this.txtPckPath.TabIndex = 7;
            // 
            // shemaCmbox
            // 
            this.shemaCmbox.FormattingEnabled = true;
            this.shemaCmbox.Location = new System.Drawing.Point(108, 51);
            this.shemaCmbox.Name = "shemaCmbox";
            this.shemaCmbox.Size = new System.Drawing.Size(146, 21);
            this.shemaCmbox.TabIndex = 8;
            this.shemaCmbox.SelectedIndexChanged += new System.EventHandler(this.shemaCmbox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Схема";
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(108, 130);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(227, 20);
            this.txtPass.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Пользователь";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Владелец";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Пароль";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Режим";
            // 
            // Patch_deployer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 236);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.shemaCmbox);
            this.Controls.Add(this.txtPckPath);
            this.Controls.Add(this.btnPckPath);
            this.Controls.Add(this.modeCmbox);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.txtOwner);
            this.Controls.Add(this.oldChkbox);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.BtnOk);
            this.Name = "Patch_deployer";
            this.Text = "Работа с патчем";
            this.Load += new System.EventHandler(this.Patch_deployer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox oldChkbox;
        private System.Windows.Forms.TextBox txtOwner;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.ComboBox modeCmbox;
        private System.Windows.Forms.Button btnPckPath;
        private System.Windows.Forms.TextBox txtPckPath;
        private System.Windows.Forms.ComboBox shemaCmbox;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

